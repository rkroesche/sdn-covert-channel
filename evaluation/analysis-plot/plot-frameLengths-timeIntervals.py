#!/usr/bin/env python
#Author: Robert Kroesche
#Email: rkroesche@sec.t-labs.tu-berlin.de

import re
import json
import glob
import itertools
from collections import OrderedDict
import matplotlib.pyplot as plt
from pprint import pprint
import Levenshtein

path = "jsons/md04_trails/no_load/"

# code below from Stack Overflow

# def sortOD(od):
#     res = OrderedDict()

#     for k, v in sorted(od.items()):
#         if isinstance(v, dict):
#             res[k] = sortOD(v)
#         else:
#             res[k] = v

#     return res

# def convertOD(od):
#     res = OrderedDict()

#     for k, v in od.items():
#         if isinstance(v, dict):
#             res[float(k)] = convertOD(v)
#         else:
#             res[int(k)] = v

#     return res


def get_data():

	# collect sent information from json files
	x = []
	frameLengths = []
	timeIntervals = []
	silentDelays = []
	numberOfRuns = []
	d = glob.glob(path + "sent*.json")
	d.sort()

	count = 0
	for i in d:
		# print "i:"
		# print str(i)
		# print i[38:40]
		if int(i[38-7:40-7]) not in frameLengths:
			frameLengths.append(int(i[38-7:40-7]))

		if i[17+27-7:18+27-7] == "-":
			if float(i[14+27-7:17+27-7]) not in timeIntervals:
				timeIntervals.append(float(i[14+27-7:17+27-7]))
		elif float(i[14+27-7:18+27-7]) not in timeIntervals:
			timeIntervals.append(float(i[14+27-7:18+27-7]))

		# print i[18+27:19+27]
		if i[18+27-7:19+27-7] == "-":
			# print i[18+27+1:19+27+1]
			if int(i[18+27+1-7:19+27+1-7]) not in silentDelays:
				silentDelays.append(int(i[18+27+1-7:19+27+1-7]))
		elif int(i[18+27-7:19+27-7]) not in silentDelays:
			# print i[18+27:19+27]
			silentDelays.append(int(i[18+27-7:19+27-7]))

		if i[19+27-7:20+27-7] == "-":
			# For 0.1
			# print i[20+27:22+27]
			if int(i[20+27-7:22+27-7]) not in numberOfRuns:
				numberOfRuns.append(int(i[20+27-7:22+27-7]))
			# count +=1
		elif i[20+27-7:21+27-7] == "-":
			# For 0.09 and smaller
			# print i[21+27:23+27]
			if int(i[21+27-7:23+27-7]) not in numberOfRuns:
				numberOfRuns.append(int(i[21+27-7:23+27-7]))
			# count +=1
	#Store the delay json data into a silentDelay dict as the json does not have delay as a key.
	# delayDict = OrderedDict.fromkeys(silentDelays, [])
	delayDict = OrderedDict.fromkeys(silentDelays, [])
	for delay in silentDelays:
		rawData = []
		print "Delay:" + str(delay)
		for i in d:
			if i[18+27-7:19+27-7] == "-":
				# print i[18+27+1:19+27+1]
				if int(i[18+27+1-7:19+27+1-7]) == delay:
					rawData.append(json.load(open(i), object_pairs_hook=OrderedDict))
					print "i:" + i
			elif int(i[18+27-7:19+27-7]) == delay:
				rawData.append(json.load(open(i), object_pairs_hook=OrderedDict))
				print "i:" + i
		delayDict[delay] = rawData

	# print count
	print "For sent files got the foll. framelengths, timeintervals, silentdelays and numberofruns"
	print frameLengths, timeIntervals, silentDelays, numberOfRuns
	print "x"
	# delayDict.pop(0)
	# pprint(delayDict)
	# exit()
	x = delayDict
	# exit()

	# Modified this for silent delays
	values = [ [[] for f in timeIntervals ] for _ in frameLengths]
	print "initialized values: " + str(values)

	sentData  = OrderedDict.fromkeys(frameLengths, None)
	j = 0
	k = 0
	l = 0
	tmp = 0

	for d in x:
		print "d:" + str(d)
		for i in x[d]:
			# if i.keys()[0] not in sentData:
			# 	sentData[i.keys()[0]] = []
			if j == len(numberOfRuns):
				j = 0
				k += 1
			if tmp == len(numberOfRuns)*len(timeIntervals):
				tmp = 0
				k = 0
				j = 0
				l += 1
			print "l, k:" + str(l) + "," + str(k)
			# Get the binary rep. of the data
			print "i.values()[0].values()[0][0]: " + str(i.values()[0].values()[0][1])
			print "values: " + str(values)
			values[l][k].append(i.values()[0].values()[0][1])
			j += 1
			tmp += 1
	print "final sent values: "
	pprint(values)
	print "sentData:"
	i = 0
	for d in sentData:
		print "d: " + str(d)
		sentData[d] = values[i]
		i += 1

	pprint(sentData)

	# collect received information from json files
	x = []
	d = glob.glob(path + "received*.json")
	d.sort()
	# for i in d:
	#
	# 	x.append(json.load(open(i), object_pairs_hook=OrderedDict))

	delayDict = OrderedDict.fromkeys(silentDelays, [])
	for delay in silentDelays:
		rawData = []
		print "Delay:" + str(delay)
		for i in d:
			if i[18 + 27 + 4-7:19 + 27 + 4-7] == "-":
				# print i[18+27+1:19+27+1]
				if int(i[18 + 27 + 1 + 4-7:19 + 27 + 1 + 4-7]) == delay:
					rawData.append(json.load(open(i), object_pairs_hook=OrderedDict))
					print "i:" + i
			elif int(i[18 + 27 + 4-7:19 + 27 + 4-7]) == delay:
				rawData.append(json.load(open(i), object_pairs_hook=OrderedDict))
				print "i:" + i
		delayDict[delay] = rawData
	# delayDict.pop(0)
	x = delayDict
	print "recv. x: "
	pprint(x)

	values = [ [[] for f in timeIntervals ] for _ in frameLengths]
	receivedData  = OrderedDict.fromkeys(frameLengths, None)
	j = 0
	k = 0
	l = 0
	tmp = 0
	for d in x:
		# print "d: " + str(d)
		for i in x[d]:
			# if i.keys()[0] not in receivedData:
			# 	receivedData[i.keys()[0]] = []
			if j == len(numberOfRuns):
				j = 0
				k += 1
			if tmp == len(numberOfRuns)*len(timeIntervals):
				tmp = 0
				k = 0
				j = 0
				l += 1
			# Get the binary rep. of the data
			values[l][k].append(i.values()[0].values()[0][1])
			j += 1
			tmp += 1
			#values.append(i.values()[0].values()[0][1])
			#values.append(re.sub(r'[\x00-\x1F,\x7F]+', '', i.values()[0].values()[0][1]))
		#values.append(re.sub(r'[\x00-\x1F]+', '', i.values()[0].values()[0][1]))

	print "receivedData: " + str(receivedData)
	print "values: " + str(values)
	i = 0
	for d in receivedData:
		print "d: " + str(d)
		receivedData[d] = values[i]
		i += 1
	print "received data: "
	pprint(receivedData)
	# exit()
	return timeIntervals, frameLengths, numberOfRuns, silentDelays, sentData, receivedData

def main():
	
	timeIntervals, frameLengths, numberOfRuns, silentDelays, sentData, receivedData = get_data()
	# print "timeIntervals"
	# print timeIntervals
	# print "frameLengths"
	# print frameLengths
	# print "numberOfRuns"
	# print numberOfRuns
	# print "sentData"
	# pprint(sentData)
	# print "receivedData"
	# print(json.dumps(receivedData, indent=4))
	# print receivedData
	# frameLengths = silentDelays

	# # Code to plot levenshtein
	# y = [[] for _ in frameLengths]
	# print y
	# frameLengths = []
	# k = 0
	# for frameLength in sentData:
	# 	frameLengths.append(frameLength)
    #
	# 	for i in range(len(sentData[frameLength])):
	# 		lev = []
	# 		for j in range(len(numberOfRuns)):
	# 			lev.append(100 * (Levenshtein.ratio(sentData[frameLength][i][j], receivedData[frameLength][i][j])))
	# 		print lev
	# 		# x = sum(lev) / len(lev)
	# 		y[k].append(lev)
	# 	k += 1
	# print "pprint y"
	# pprint(y)
	# print frameLengths
	# print "Now plot: "
	# plot(timeIntervals, y, frameLengths)

	print "Now print error rates:"
	errors = ["MSB", "EOM"]
	errorDict = dict.fromkeys(errors, dict.fromkeys(frameLengths, dict.fromkeys(timeIntervals, 0)))
	# pprint(errorDict)
	# exit()
	globalMSB = 0
	frameLengthDict = dict.fromkeys(frameLengths, None)
	for frameLength in frameLengths:
		print "frameLength:" + str(frameLength)
		timeIntervalDict = dict.fromkeys(timeIntervals, 0)
		for interval in range(len(timeIntervals)):
			print "interval, timeIntervals[interval]"
			print interval, timeIntervals[interval]
			localMSB=0
			for run in range(len(numberOfRuns)):
				for bitValues in receivedData[frameLength][interval][run]:
					if bitValues == u'':
						localMSB += 1
						globalMSB += 1
			print "localMSB for: " + str(frameLength) + " " + str(timeIntervals[interval]) + " is: " + str(localMSB)
			timeIntervalDict[timeIntervals[interval]] = localMSB
		frameLengthDict[frameLength] = timeIntervalDict
	errorDict["MSB"] = frameLengthDict
	print "globalMSB errors are:"
	print globalMSB
	# print "final errorDict with MSBs:"
	# pprint(errorDict)

	globalEOM = 0
	frameLengthDict = dict.fromkeys(frameLengths, None)
	for frameLength in frameLengths:
		print "frameLength:" + str(frameLength)
		timeIntervalDict = dict.fromkeys(timeIntervals, 0)
		for interval in range(len(timeIntervals)):
			print "interval, timeIntervals[interval]"
			print interval, timeIntervals[interval]
			localEOM=0
			for run in range(len(numberOfRuns)):
				correctFrames = 0
				if len(receivedData[frameLength][interval][run]) < len(sentData[frameLength][interval][run]):
					print "received frames less than sent, got: " + str(len(receivedData[frameLength][interval][run]))
					print "sent frames: " + str(len(sentData[frameLength][interval][run]))
					for bitValues in receivedData[frameLength][interval][run]:
						if bitValues != u'':
							correctFrames += 1
					localEOM += 1
					globalEOM += 1
			print "localEOM for: " + str(frameLength) + " " + str(timeIntervals[interval]) + " is: " + str(localEOM)
			timeIntervalDict[timeIntervals[interval]] = localEOM
		frameLengthDict[frameLength] = timeIntervalDict
	errorDict["EOM"] = frameLengthDict
	print "globalEOM errors are:"
	print globalEOM
	print "final errorDict with EOMs:"
	pprint(errorDict)

	print "In percentage of total frames sent, the error rates are:"
	totalFrames = {7: 64, 14: 32, 28: 16}
	for error in errorDict:
		for frameLength in frameLengths:
			for interval in timeIntervals:
				if error == "MSB":
					errorDict[error][frameLength][interval] = float(errorDict[error][frameLength][interval])/(totalFrames[frameLength]*len(numberOfRuns)) * 100
				elif error == "EOM":
					errorDict[error][frameLength][interval] = float(errorDict[error][frameLength][interval])/len(numberOfRuns) * 100
	pprint(errorDict)

def plot(x,y,frameLengths):
	x = [i * 1000 for i in x]
	marker = itertools.cycle(('_', '1','|','2','x','3'))
	color = itertools.cycle(("#9b59b6", "#3498db", "#95a5a6", "#e74c3c", "#34495e", "#2ecc71"))

	fig = plt.figure(1, frameon=True)
	fig.subplots_adjust(bottom=0.2)
	ax = plt.subplot(111)
	ax.yaxis.grid()
	i = 0
	dataToPlot = []
	frameLen1 = y[0]
	frameLen2 = y[1]
	frameLen3 = y[2]
	for timeInterval in range(0, len(x)):
		allFramesForTimeInterval = []
		allFramesForTimeInterval.append(frameLen1[timeInterval])
		allFramesForTimeInterval.append(frameLen2[timeInterval])
		allFramesForTimeInterval.append(frameLen3[timeInterval])

		# allFramesForTimeInterval = [frameLen1[timeInterval],
		# 							frameLen2[timeInterval],
		# 							frameLen3[timeInterval]]
		dataToPlot.extend(allFramesForTimeInterval)
	print "dataToPlot: "
	pprint(dataToPlot)
	bp = ax.boxplot(dataToPlot, sym='+', vert=1, whis=1.5)
	plt.setp(bp['fliers'], color='blue', marker='+', markersize=3)
	# pprint(realDataToPlot)
	# for data in dataToPlot:
	# 	print "Data: "
	# 	realDataToPlot.append(data)
		# plt.boxplot(data)
	# plt.boxplot(realDataToPlot[0])
		# break
	#plt.yticks(p, l)
	#ax.set_yscale('log')
	#plt.xticks(x[:])
	plt.title('Levenshtein ratio for message length 64 with load')
	# plt.ylim((0,110))
	# plt.xlim((min(x[:]) - 10, max(x[:]) + 10))
	plt.ylabel('Accuracy [%]')
	plt.xlabel("Time Interval [ms]")
	# Legend stuff from Tobias
	# ax.legend( loc='lower center', ncol = 3, bbox_to_anchor=(0.5, -0.5), numpoints=1)
	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0 + box.height * 0.25 , box.width * 1.0, box.height * 0.75])
	ax.set_axisbelow(True)
	figureName="plotBox-accuracy-noLoad-offset5.pdf"
	plt.savefig(figureName,dpi = (320),format='pdf')

main()