#!/usr/bin/env python
#Author: Robert Kroesche
#Email: rkroesche@sec.t-labs.tu-berlin.de

import re
import json
import glob
import itertools
from collections import OrderedDict
import matplotlib
import matplotlib.pyplot as plt
from pprint import pprint
import Levenshtein

path = "jsons/md04_trails/no_load/"

# code below from Stack Overflow

# def sortOD(od):
#     res = OrderedDict()

#     for k, v in sorted(od.items()):
#         if isinstance(v, dict):
#             res[k] = sortOD(v)
#         else:
#             res[k] = v

#     return res

# def convertOD(od):
#     res = OrderedDict()

#     for k, v in od.items():
#         if isinstance(v, dict):
#             res[float(k)] = convertOD(v)
#         else:
#             res[int(k)] = v

#     return res


def get_data():

	# collect sent information from json files
	x = []
	frameLengths = []
	timeIntervals = []
	numberOfRuns = []
	d = glob.glob(path + "sent*.json")
	d.sort()

	for i in d:
		
		if int(i[11+20:13+20]) not in frameLengths:
			frameLengths.append(int(i[11+20:13+20]))

		if i[17+20:18+20] == "-":
			if float(i[14+20:17+20]) not in timeIntervals:
				timeIntervals.append(float(i[14+20:17+20]))
		elif float(i[14+20:18+20]) not in timeIntervals:
			timeIntervals.append(float(i[14+20:18+20]))
		
		if i[17+20:18+20] == "-":
			if int(i[18+20:20+20]) not in numberOfRuns:
				numberOfRuns.append(int(i[18+20:20+20]))
		
		elif int(i[19+20:21+20]) not in numberOfRuns:
			numberOfRuns.append(int(i[19+20:21+20]))
		
		x.append(json.load(open(i), object_pairs_hook=OrderedDict))

	values = [ [[] for f in timeIntervals ] for _ in frameLengths]

	sentData  = OrderedDict()
	j = 0
	k = 0
	l = 0
	tmp = 0
	for i in x: 
		
		if i.keys()[0] not in sentData:
			sentData[i.keys()[0]] = []
		if j == len(numberOfRuns):
			j = 0
			k += 1
		if tmp == len(numberOfRuns)*len(timeIntervals):
			tmp = 0
			k = 0
			j = 0
			l += 1  
		values[l][k].append(i.values()[0].values()[0][1])
		j += 1
		tmp += 1
 
	i = 0
	for d in sentData:
		
		sentData[d] = values[i]
		i += 1

	# collect received information from json files
	x = []
	d = glob.glob(path + "received*.json")
	d.sort()
	for i in d:
		
		x.append(json.load(open(i), object_pairs_hook=OrderedDict))

	values = [ [[] for f in timeIntervals ] for _ in frameLengths]
	receivedData  = OrderedDict()
	j = 0
	k = 0
	l = 0
	tmp = 0
	for i in x: 
		if i.keys()[0] not in receivedData:
			receivedData[i.keys()[0]] = []
		if j == len(numberOfRuns):
			j = 0
			k += 1
		if tmp == len(numberOfRuns)*len(timeIntervals):
			tmp = 0
			k = 0
			j = 0
			l += 1  
		values[l][k].append(i.values()[0].values()[0][1])
		j += 1
		tmp += 1
		#values.append(i.values()[0].values()[0][1])
		#values.append(re.sub(r'[\x00-\x1F,\x7F]+', '', i.values()[0].values()[0][1]))
		#values.append(re.sub(r'[\x00-\x1F]+', '', i.values()[0].values()[0][1]))

	i = 0
	for d in receivedData:
		
		receivedData[d] = values[i]
		i += 1

	return timeIntervals, frameLengths, numberOfRuns, sentData, receivedData

def main():
	
	timeIntervals, frameLengths, numberOfRuns, sentData, receivedData = get_data()
	print "timeIntervals"
	print timeIntervals
	print "frameLengths"
	print frameLengths
	print "numberOfRuns"
	print numberOfRuns
	print "sentData"
	pprint(sentData)
	print "receivedData"
	pprint(receivedData)
	y = [[] for _ in frameLengths]
	print y
	frameLengths = []
	k = 0
	for frameLength in sentData:
		frameLengths.append(frameLength)
		
		for i in range(len(sentData[frameLength])):
			lev = []
			for j in range(len(numberOfRuns)):
				lev.append(100*(Levenshtein.ratio(sentData[frameLength][i][j], receivedData[frameLength][i][j])))
			print lev
			# x = sum(lev) / len(lev)
			y[k].append(lev)
		k += 1
	print "pprint y"
	pprint(y)
	print frameLengths

	plot(timeIntervals, y, frameLengths)

def plot(x,y,frameLengths):
	x = [i * 1000 for i in x]
	marker = itertools.cycle(('_', '1','|','2','x','3'))
	color = itertools.cycle(("#9b59b6", "#3498db", "#95a5a6", "#e74c3c", "#34495e", "#2ecc71"))

	fig = plt.figure(1, frameon=True)
	fig.subplots_adjust(bottom=0.2)
	ax = plt.subplot(111)
	ax.yaxis.grid()
	i = 0
	dataToPlot = []
	frameLen1 = y[0]
	frameLen2 = y[1]
	frameLen3 = y[2]
	for timeInterval in range(0, len(x)):
		allFramesForTimeInterval = []
		allFramesForTimeInterval.append(frameLen1[timeInterval])
		allFramesForTimeInterval.append(frameLen2[timeInterval])
		allFramesForTimeInterval.append(frameLen3[timeInterval])

		# allFramesForTimeInterval = [frameLen1[timeInterval],
		# 							frameLen2[timeInterval],
		# 							frameLen3[timeInterval]]
		dataToPlot.extend(allFramesForTimeInterval)
	print "dataToPlot: "
	pprint(dataToPlot)
	bp = ax.boxplot(dataToPlot, sym='+', vert=1, whis=1.5)
	plt.setp(bp['fliers'], color='blue', marker='+', markersize=3)
	# pprint(realDataToPlot)
	# for data in dataToPlot:
	# 	print "Data: "
	# 	realDataToPlot.append(data)
		# plt.boxplot(data)
	# plt.boxplot(realDataToPlot[0])
		# break
	#plt.yticks(p, l)
	#ax.set_yscale('log')
	#plt.xticks(x[:])
	plt.title('Levenshtein ratio for message length 64 with load')
	# plt.ylim((0,110))
	# plt.xlim((min(x[:]) - 10, max(x[:]) + 10))
	plt.ylabel('Accuracy [%]')
	plt.xlabel("Time Interval [ms]")
	# Legend stuff from Tobias
	# ax.legend( loc='lower center', ncol = 3, bbox_to_anchor=(0.5, -0.5), numpoints=1)
	# box = ax.get_position()
	# ax.set_position([box.x0, box.y0 + box.height * 0.25 , box.width * 1.0, box.height * 0.75])
	ax.set_axisbelow(True)
	figureName="plotBox-accuracy-noLoad.pdf"
	plt.savefig(figureName,dpi = (320),format='pdf')

main()