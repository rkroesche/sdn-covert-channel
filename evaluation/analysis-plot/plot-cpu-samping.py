#!/usr/bin/env python
import sys, getopt
import os
import re
import json
from pprint import pprint
#import plotly
#print plotly.__version__  # version >1.9.4 required
#import plotly.graph_objs as go
import numpy as np
import matplotlib.pyplot as plt
from pylab import *

# plt.rc('font', family='serif', serif='Times')
# plt.rc('text', usetex=True)
# plt.rc('xtick', labelsize=8)
# plt.rc('ytick', labelsize=8)
# plt.rc('axes', labelsize=8)
# plt.rc('axes', linewidth=.5)
# plt.rc('lines', linewidth=.5)
# plt.rc('figure', figsize=(width, height))

#resultKeys = ['0.1', '0.09', '0.08', '0.07', '0.06', '0.05', '0.04', '0.03']
resultKeys = ['0.1']
frameLength = '7'
# resultKeys = ['60M','70M']
#resultKeys = ['10M', '20M', '30M', '40M', '50M', '60M', '70M', '80M', '90M', '100M']
xAxisTimeLine = range(1, 100)
#  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
#12578 onos      20   0 3210988 122948  23324 S  99,7  3,1   0:02.00 java
#   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
# 32302 root      20   0 6391964 466064  22828 S  37.9  2.8   2:15.45 java
cpuPattern = "\d+\s+root\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\w+\s+\d+,\d+"
pattern = "\d+\s+root\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\w+\s+\d+.\d+\s+\d+.\d+"
markers = ['None', 'None', 'None', 'None', 'None', 'o', 'v']
markers = ['1', '2', '3', '4', '.', 'x', '+']
markers = ['*', 'x', '+']
markerColours = ['#097054', '#FFDE00', '#6599FF', '#FF9900', '#217C7E', '#9A3334', '#E86850']
markerColours = ['#627894', '#466289', '#6D8CA0', '#A0AEC1', '#BACFE4', '#B4D2E7', '#D4DDEF']
markerColours = ['#D4DDEF', '#B4D2E7', '#BACFE4', '#A0AEC1', '#6D8CA0', '#466289', '#627894']
markerColours = [u'#64b5cd', u'#55a868', u'#c44e52', u'#8172b2', u'#ccb974', u'#4c72b0']
# Blues sequential
markerColours = [u'#dbe9f6', u'#bad6eb', u'#89bedc', u'#539ecd', u'#2b7bba', u'#0b559f']
markerColours = [u'#d9e9f6', u'#bad6eb', u'#89bedc', u'#539ecd', u'#2b7bba', u'#0b559f', u'#097054', u'#FFDE00']
fontsize = 8
colors = ['#3D9970', '#FF9136', '#FFC51B']

noLoadTopPath='onosCpuMem/noLoad/'
withLoadTopPath='onosCpuMem/withLoad/'
ofcprobeToPath='onosCpuMem/ofcprobe/'
noLoadCpuResultFileName='switchID-cpu-1024-7-100ms-noLoad.pdf'
noLoadMemoryResultFileName='switchID-memory-1024-7-100ms-noLoad.pdf'

withLoadCpuResultFileName='switchID-cpu-1024-7-100ms-withLoad.pdf'
withLoadMemoryResultFileName='switchID-memory-1024-7-100ms-withLoad.pdf'

CpuResultFileName='../Thesis/images/switchID-cpu-1024-7-100ms.pdf'
MemoryResultFileName='switchID-mem-1024-7-100ms.pdf'

def getResultFiles(path):
    fileList = []
    for root, directories, filenames in os.walk(path, 'r'):
        for filename in filenames:
            fileList.append(os.path.join(root, filename))
    return fileList

def getDictWithCpuAndMem(fileList):
    tempDict = dict.fromkeys(resultKeys, None)
    for file in fileList:
        values = []
        if re.search('onosCpuMem', file):
            fileHandle = open(file, 'r')
            fileData = fileHandle.read()
            fileHandle.close()
            matches = re.findall(pattern, fileData)
            if re.search('0.1-' + frameLength, file):
                if tempDict.get('0.1') is None:
                    values.append(matches)
                    tempDict['0.1'] = values
                else:
                    values = tempDict.get('0.1')
                    values.append(matches)
                    tempDict['0.1'] = values
            elif re.search('0.09-' + frameLength, file):
                if tempDict.get('0.09') is None:
                    values.append(matches)
                    tempDict['0.09'] = values
                else:
                    values = tempDict.get('0.09')
                    values.append(matches)
                    tempDict['0.09'] = values
            elif re.search('0.08-' + frameLength, file):
                if tempDict.get('0.08') is None:
                    values.append(matches)
                    tempDict['0.08'] = values
                else:
                    values = tempDict.get('0.08')
                    values.append(matches)
                    tempDict['0.08'] = values
            elif re.search('0.07-' + frameLength, file):
                if tempDict.get('0.07') is None:
                    values.append(matches)
                    tempDict['0.07'] = values
                else:
                    values = tempDict.get('0.07')
                    values.append(matches)
                    tempDict['0.07'] = values
            elif re.search('0.06-' + frameLength, file):
                if tempDict.get('0.06') is None:
                    values.append(matches)
                    tempDict['0.06'] = values
                else:
                    values = tempDict.get('0.06')
                    values.append(matches)
                    tempDict['0.06'] = values
            elif re.search('0.05-' + frameLength, file):
                if tempDict.get('0.05') is None:
                    values.append(matches)
                    tempDict['0.05'] = values
                else:
                    values = tempDict.get('0.05')
                    values.append(matches)
                    tempDict['0.05'] = values
            elif re.search('0.04-' + frameLength, file):
                if tempDict.get('0.04') is None:
                    values.append(matches)
                    tempDict['0.04'] = values
                else:
                    values = tempDict.get('0.04')
                    values.append(matches)
                    tempDict['0.04'] = values
            elif re.search('0.03-' + frameLength, file):
                if tempDict.get('0.03') is None:
                    values.append(matches)
                    tempDict['0.03'] = values
                else:
                    values = tempDict.get('0.03')
                    values.append(matches)
                    tempDict['0.03'] = values
    return tempDict

def parseCpu(d):
    tempDict = dict.fromkeys(resultKeys, None)
    if d.has_key('ofcOnly'):
        tempDict['ofcOnly'] = []
    for k in d.keys():
        valuesList = []
        valuesList = d.get(k)
        if valuesList == None:
            print('No values for ' + k + 'key')
            continue
        cpuList = []
        for l in valuesList:
            cpuTrialList = []
            for v in l:
                #for v in valuesList[0]:
                #Remove multiple whitespaces first
                subString = re.sub('\s+', ' ', str(v))
                #Then split the string based on a whitespace
                #The 9th position is the CPU and 10th is the Memory
                splitString = subString.split(' ')
                #print('cpu used:' + str(splitString[8]))
                #print('mem used:' + str(splitString[9]))
                #print subString
                cpuValue = str(splitString[8]).replace(',','.')
                #print cpuValue
                cpuTrialList.append(cpuValue)
            cpuList.append(cpuTrialList)
        tempDict[k] = cpuList
    return tempDict

def parseMem(d):
    tempDict = dict.fromkeys(resultKeys, None)
    if d.has_key('ofcOnly'):
        tempDict['ofcOnly'] = []
    for k in d.keys():
        valuesList = []
        valuesList = d.get(k)
        if valuesList == None:
            print('No values for ' + k + 'key')
            continue
        memList = []
        for l in valuesList:
            memTrialList = []
            for v in l:
                #Remove multiple whitespaces first
                subString = re.sub('\s+', ' ', str(v))
                #Then split the string based on a whitespace
                #The 9th position is the CPU and 10th is the Memory
                splitString = subString.split(' ')
                #print('cpu used:' + str(splitString[8]))
                #print('mem used:' + str(splitString[9]))
                #print subString
                memValue = str(splitString[9]).replace(',','.')
                # print memValue
                memTrialList.append(memValue)
            memList.append(memTrialList)
        tempDict[k] = memList
    return tempDict

def getAvgCpuResults(cpuResultsDict, withLoad=False):
    allCpuResults = dict.fromkeys(resultKeys, None)
    if cpuResultsDict.has_key('ofcOnly'):
        allCpuResults['ofcOnly'] = []
    for k in resultKeys:
        averageCpuResults = []
        cpuResults = cpuResultsDict[k]
        if cpuResults == None:
            print('No cpuResults for ' + k + 'key')
            continue
        count = len(cpuResults)
        print "count is:" + str(count)
        print "cpuResults[key]" + str(len(cpuResults[0]))
        if withLoad is False:
            #### START TIME IS FROM 75+60 SECOND MARK
            #### END TIME IS FROM 75+60+600 SECOND MARK
            max = 1060  # len(cpuResults[0])
            # for i in range(70+60, 70+60+600):
            for i in range(60, 1085):
                totalPerSecondCpu = 0
                for trial in range(0, 1):
                # for trial in range(0, count):
                    totalPerSecondCpu += float(cpuResults[trial][i])
                averagePerSecondCpu = totalPerSecondCpu / 1
                # averagePerSecondCpu = totalPerSecondCpu / count
                averageCpuResults.append(averagePerSecondCpu)
            allCpuResults[k] = averageCpuResults
        elif withLoad is True:
            #### START TIME IS FROM 75+60+60 SECOND MARK
            #### END TIME IS FROM 75+60+60+600 SECOND MARK
            max = 821  # len(cpuResults[0])
            for i in range(60, 1085):
            # for i in range(99+38, 699+38):
                totalPerSecondCpu = 0
                for trial in range(0, 1):
                # for trial in range(0, count):
                    totalPerSecondCpu += float(cpuResults[trial][i])
                averagePerSecondCpu = totalPerSecondCpu / 1
                # averagePerSecondCpu = totalPerSecondCpu / count
                averageCpuResults.append(averagePerSecondCpu)
            allCpuResults[k] = averageCpuResults
    return allCpuResults

    # allCpuResults = dict.fromkeys(resultKeys, None)
    # if cpuResultsDict.has_key('ofcOnly'):
    #     allCpuResults['ofcOnly'] = []
    # for k in resultKeys:
    #     averageCpuResults = []
    #     cpuResults = cpuResultsDict[k]
    #     if cpuResults == None:
    #         print('No cpuResults for ' + k + 'key')
    #         continue
    #     count = len(cpuResults)
    #     print "count is:" + str(count)
    #     if withLoad is False:
    #         for i in range(70+60, 70+60+600):
    #             totalPerSecondCpu = 0
    #             for trial in range(0, 1):
    #             # for trial in range(0, count):
    #                 totalPerSecondCpu += float(cpuResults[trial][i])
    #             averagePerSecondCpu = totalPerSecondCpu / 1
    #             # averagePerSecondCpu = totalPerSecondCpu / count
    #             averageCpuResults.append(averagePerSecondCpu)
    #         allCpuResults[k] = averageCpuResults
    #     elif withLoad is True:
    #         for i in range(70+60+60, 70+60+600+60):
    #             totalPerSecondCpu = 0
    #             for trial in range(0, 1):
    #             # for trial in range(0, count):
    #                 totalPerSecondCpu += float(cpuResults[trial][i])
    #             averagePerSecondCpu = totalPerSecondCpu / 1
    #             # averagePerSecondCpu = totalPerSecondCpu / count
    #             averageCpuResults.append(averagePerSecondCpu)
    #         allCpuResults[k] = averageCpuResults
    # return allCpuResults

def getAvgMemResults(memResultsDict, withLoad=False):
    allMemResults = dict.fromkeys(resultKeys, None)
    if memResultsDict.has_key('ofcOnly'):
        allMemResults['ofcOnly'] = []
    for k in resultKeys:
        averageMemResults = []
        memResults = memResultsDict[k]
        if memResults == None:
            print('No memResults for ' + k + 'key')
            continue
        count = len(memResults)
        if withLoad is False:
            for i in range(70+60, 70+60+600):
                # print i
                totalPerSecondMem = 0
                for trial in range(0, 1):
                # for trial in range(0, count):
                    totalPerSecondMem += float(memResults[trial][i])
                averagePerSecondMem = totalPerSecondMem / 1
                # averagePerSecondMem = totalPerSecondMem / count
                averageMemResults.append(averagePerSecondMem)
            allMemResults[k] = averageMemResults
        elif withLoad is True:
            for i in range(70+60+60, 70+60+600+60):
                totalPerSecondMem = 0
                for trial in range(0, 1):
                # for trial in range(0, count):
                    totalPerSecondMem += float(memResults[trial][i])
                averagePerSecondMem = totalPerSecondMem / 1
                # averagePerSecondMem = totalPerSecondMem / count
                averageMemResults.append(averagePerSecondMem)
            allMemResults[k] = averageMemResults
    return allMemResults

def cpuAndMemory():
    print('Get CPU data.')
    # no load, covert channel only
    noLoadFileList = getResultFiles(noLoadTopPath)
    noLoadCpuMemDict = getDictWithCpuAndMem(noLoadFileList)
    noLoadCpuResultsDict = parseCpu(noLoadCpuMemDict)
    noLoadavgCpuResultsDict = getAvgCpuResults(noLoadCpuResultsDict)
    
    # with load and covert channel
    withLoadFileList = getResultFiles(withLoadTopPath)
    withLoadCpuMemDict = getDictWithCpuAndMem(withLoadFileList)
    withLoadCpuResultsDict = parseCpu(withLoadCpuMemDict)
    withLoadavgCpuResultsDict = getAvgCpuResults(withLoadCpuResultsDict, withLoad=True)

    # OFCProbe only
    ofcOnlyFileList = getResultFiles(ofcprobeToPath)
    ofcOnlyCpuMemDict = getDictWithCpuAndMem(ofcOnlyFileList)
    ofcOnlyCpuResultsDict = parseCpu(ofcOnlyCpuMemDict)
    ofcOnlyavgCpuResultsDict = getAvgCpuResults(ofcOnlyCpuResultsDict)
 
    cpu = []
    #mem = []
    for key in resultKeys:
        cpu.append(ofcOnlyavgCpuResultsDict[key])
    for key in resultKeys:
        cpu.append(noLoadavgCpuResultsDict[key])
    for key in resultKeys:
        cpu.append(withLoadavgCpuResultsDict[key])
    #for key in resultKeys:
        #mem.append(noLoadavgMemResultsDict[key])
    #for key in resultKeys:
        #mem.append(withLoadavgMemResultsDict[key])
    print cpu
    #print mem
    #################################################
    #### CPU stuff
    fig = plt.figure(1, frameon=True)
    ax = plt.subplot(111)
    medianpointprops = dict(marker='', linestyle='-', color='red')
    bp = plt.boxplot(cpu, sym='+', vert=1, whis=1.5, patch_artist=True, medianprops=medianpointprops)

    for line in bp['medians']:
        x, y = line.get_xydata()[1] # top of median line
        print x, y 
        text(x, y, '%.1f' % y)
    # For side by side colours
    # colors = ['#3D9970', '#FF9136']
    # colors = ['#3D9970', '#FF9136', '#FFC51B']
    k = 0
    for patch in bp['boxes']:
        i = k % 2
        patch.set_facecolor(colors[i])
        plt.setp(bp['whiskers'], color='blue')
        plt.setp(bp['fliers'], color='blue')
        k += 1
    # For separate colours
    i=0
    for patch in bp['boxes']:
        
        patch.set_facecolor(colors[i])

        plt.setp(bp['whiskers'], color='black')
        plt.setp(bp['fliers'], color='blue')
        patch.set(linewidth=.5)
        i += 1
    #plt.xlabel('Time interval [ms]')
    plt.ylabel("CPU Usage (\%)")
    c = 0
    #ax.text(0.875, 110, u'OFCProbe', fontsize=8)
    #ax.text(1.82, 110, u'covert channel', fontsize=8)
    #ax.text(2.65, 110, u'OFCProbe + covert channel', fontsize=8)
    offset = 1.5
    plt.plot([offset, offset], [0, 100], color='#000000')
    plt.plot([offset+1.0, offset+1.0], [0, 100], color='#000000')
    # For side by side
    # xmark=['                        10M', '', '                        20M', '',
    #        '                        30M', '', '                        40M', '',
    #        '                        50M', '', '                        60M', '',
    #        '                        70M', '']
    # For separate
    xmark=['OFCProbe only', 'Covert Channel only', 'OFCProbe + Covert Channel']
    plt.xticks(range(1, 4), tuple(xmark))
    #plt.yticks(range(0 ,110,5))
    box=ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.15 , box.width * 1.0, box.height * 0.85])
    ax.yaxis.grid(True, linestyle='-', which='major', color='grey', alpha=.5)
    ax.set_axisbelow(True)
    # For side by side
    # plt.figtext(0.40, 0.0001, 'No load', backgroundcolor=colors[0], color='black', weight='roman', size=fontsize)
    # plt.figtext(0.50, 0.0001, 'With load', backgroundcolor=colors[1], color='black', weight='roman', size=fontsize)
    # plt.figtext(0.60, 0.0001, 'p=0.0139',
    #             backgroundcolor=colors[2], color='black', weight='roman',
    #             size=fontsize)
    # plt.tick_params(labelsize=fontsize)
    plt.savefig(CpuResultFileName, format='pdf')
    plt.close()
    exit()

    #################################################
    #### Memory stuff
    fig = plt.figure(1, figsize=(12, 7), frameon=True)
    ax = plt.subplot(111)
    bp = plt.boxplot(mem, patch_artist=True)
    # For side by side colours
    # colors = ['#3D9970', '#FF9136']
    # colors = ['#3D9970', '#FF9136', '#FFC51B']
    i = 0
    for patch in bp['boxes']:
        
        patch.set_facecolor(colors[i])
        plt.setp(bp['whiskers'], color='blue')
        plt.setp(bp['fliers'], color='blue')
        i += 1
    # For separate colours
    k = 0
    for patch in bp['boxes']:
        if k < 7:
            patch.set_facecolor(colors[0])
        if k > 6:
            patch.set_facecolor(colors[1])
        plt.setp(bp['whiskers'], color='black')
        plt.setp(bp['fliers'], color='black')
        patch.set(linewidth=.5)
        k += 1
    plt.xlabel('Tx Throughput (Mbps)')
    plt.ylabel('Memory usage (MB)')
    c = 0
    ax.text(0.963, 4.10, u'No load', fontsize=8)
    ax.text(1.952, 4.10, u'With load', fontsize=8)
    offset = 1.5
    plt.plot([offset, offset], [0, 4.0], color='#000000')
 
    # For side by side
    # xmark=['                        10M', '', '                        20M', '',
    #        '                        30M', '', '                        40M', '',
    #        '                        50M', '', '                        60M', '',
    #        '                        70M', '']
    # For separate
    xmark=['100', '100']
    plt.xticks(range(1, 4), tuple(xmark))
    box=ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.15 , box.width * 1.0, box.height * 0.85])
    ax.yaxis.grid(True, linestyle='-', which='major', color='grey', alpha=.5)
    ax.set_axisbelow(True)
    # For side by side
    # plt.figtext(0.40, 0.0001, 'No load', backgroundcolor=colors[0], color='black', weight='roman', size=fontsize)
    # plt.figtext(0.50, 0.0001, 'With load', backgroundcolor=colors[1], color='black', weight='roman', size=fontsize)
    # plt.figtext(0.60, 0.0001, 'p=0.0139',
    #             backgroundcolor=colors[2], color='black', weight='roman',
    #             size=fontsize)
    # plt.tick_params(labelsize=fontsize)
    plt.savefig(MemoryResultFileName, format='pdf', bbox_inches='tight')
    plt.close()
    #
    # #OFCProbe only results
    # cpuResultsDict = parseCpu(ofcOnlyCpuMemDict)
    # list = []
    # index = 0
    # for l in cpuResultsDict['ofcOnly'][0]:
    #     index += 1
    #     if index > 99 and index < 700:
    #         list.append(l)
    #     if index > 700:
    #         break
    #
    # #OFCProbe only results using matplotlib
    # xAxis = range(0, 600)
    # xAxis = np.array(xAxis)
    # index=0
    # plt.plot(figsize=(12,7))
    # for key in resultKeys:
    #     plt.plot(xAxis, list, label='Tx at ' + key + 'bps', linestyle='',\
    #              marker=markers[index], markersize=7, color=markerColours[index])
    #     index += 1
    # # plt.legend(bbox_to_anchor=(1, 1), loc=2, shadow=True, fontsize='small', borderaxespad=1)
    # plt.ylabel('CPU usage (%)')
    # plt.xlabel('Time (s)')
    # plt.savefig('EuroSP-cpu-OfcOnly-Load.pdf', format='pdf', bbox_inches='tight')
    # plt.close()

if __name__ == "__main__":
    cpuAndMemory()