#!/usr/bin/env python
#Author: Robert Kroesche
#Email: rkroesche@sec.t-labs.tu-berlin.de

import re
import json
import glob
import itertools
from collections import OrderedDict
import matplotlib.pyplot as plt
from pprint import pprint
import Levenshtein

path = "jsons/md04_trails/no_load/"

# code below from Stack Overflow

# def sortOD(od):
#     res = OrderedDict()

#     for k, v in sorted(od.items()):
#         if isinstance(v, dict):
#             res[k] = sortOD(v)
#         else:
#             res[k] = v

#     return res

# def convertOD(od):
#     res = OrderedDict()

#     for k, v in od.items():
#         if isinstance(v, dict):
#             res[float(k)] = convertOD(v)
#         else:
#             res[int(k)] = v

#     return res


def get_data():

	# collect sent information from json files
	x = []
	frameLengths = []
	timeIntervals = []
	silentDelays = []
	numberOfRuns = []
	d = glob.glob(path + "sent*.json")
	d.sort()

	count = 0
	for i in d:
		# print "i:"
		# print str(i)
		# print i[38:40]
		if int(i[38-7:40-7]) not in frameLengths:
			frameLengths.append(int(i[38-7:40-7]))

		if i[17+27-7:18+27-7] == "-":
			if float(i[14+27-7:17+27-7]) not in timeIntervals:
				timeIntervals.append(float(i[14+27-7:17+27-7]))
		elif float(i[14+27-7:18+27-7]) not in timeIntervals:
			timeIntervals.append(float(i[14+27-7:18+27-7]))

		# print i[18+27:19+27]
		if i[18+27-7:19+27-7] == "-":
			# print i[18+27+1:19+27+1]
			if int(i[18+27+1-7:19+27+1-7]) not in silentDelays:
				silentDelays.append(int(i[18+27+1-7:19+27+1-7]))
		elif int(i[18+27-7:19+27-7]) not in silentDelays:
			# print i[18+27:19+27]
			silentDelays.append(int(i[18+27-7:19+27-7]))

		if i[19+27-7:20+27-7] == "-":
			# For 0.1
			# print i[20+27:22+27]
			if int(i[20+27-7:22+27-7]) not in numberOfRuns:
				numberOfRuns.append(int(i[20+27-7:22+27-7]))
			# count +=1
		elif i[20+27-7:21+27-7] == "-":
			# For 0.09 and smaller
			# print i[21+27:23+27]
			if int(i[21+27-7:23+27-7]) not in numberOfRuns:
				numberOfRuns.append(int(i[21+27-7:23+27-7]))
			# count +=1
	#Store the delay json data into a silentDelay dict as the json does not have delay as a key.
	# delayDict = OrderedDict.fromkeys(silentDelays, [])
	delayDict = OrderedDict.fromkeys(silentDelays, [])
	for delay in silentDelays:
		rawData = []
		print "Delay:" + str(delay)
		for i in d:
			if i[18+27-7:19+27-7] == "-":
				# print i[18+27+1:19+27+1]
				if int(i[18+27+1-7:19+27+1-7]) == delay:
					rawData.append(json.load(open(i), object_pairs_hook=OrderedDict))
					print "i:" + i
			elif int(i[18+27-7:19+27-7]) == delay:
				rawData.append(json.load(open(i), object_pairs_hook=OrderedDict))
				print "i:" + i
		delayDict[delay] = rawData

	# print count
	print "For sent files got the foll. framelengths, timeintervals, silentdelays and numberofruns"
	print frameLengths, timeIntervals, silentDelays, numberOfRuns
	print "x"
	# delayDict.pop(0)
	# pprint(delayDict)
	# exit()
	x = delayDict
	# exit()

	# Modified this for silent delays
	values = [ [[] for f in timeIntervals ] for _ in frameLengths]
	print "initialized values: " + str(values)

	sentData  = OrderedDict.fromkeys(frameLengths, None)
	j = 0
	k = 0
	l = 0
	tmp = 0

	for d in x:
		print "d:" + str(d)
		for i in x[d]:
			# if i.keys()[0] not in sentData:
			# 	sentData[i.keys()[0]] = []
			if j == len(numberOfRuns):
				j = 0
				k += 1
			if tmp == len(numberOfRuns)*len(timeIntervals):
				tmp = 0
				k = 0
				j = 0
				l += 1
			print "l, k:" + str(l) + "," + str(k)
			# Get the binary rep. of the data
			print "i.values()[0].values()[0][0]: " + str(i.values()[0].values()[0][0])
			print "values: " + str(values)
			values[l][k].append(i.values()[0].values()[0][0])
			j += 1
			tmp += 1
	print "final sent values: "
	pprint(values)
	print "sentData:"
	i = 0
	for d in sentData:
		print "d: " + str(d)
		sentData[d] = values[i]
		i += 1

	pprint(sentData)

	# collect received information from json files
	x = []
	d = glob.glob(path + "received*.json")
	d.sort()
	# for i in d:
	#
	# 	x.append(json.load(open(i), object_pairs_hook=OrderedDict))

	delayDict = OrderedDict.fromkeys(silentDelays, [])
	for delay in silentDelays:
		rawData = []
		print "Delay:" + str(delay)
		for i in d:
			if i[18 + 27 + 4-7:19 + 27 + 4-7] == "-":
				# print i[18+27+1:19+27+1]
				if int(i[18 + 27 + 1 + 4-7:19 + 27 + 1 + 4-7]) == delay:
					rawData.append(json.load(open(i), object_pairs_hook=OrderedDict))
					print "i:" + i
			elif int(i[18 + 27 + 4-7:19 + 27 + 4-7]) == delay:
				rawData.append(json.load(open(i), object_pairs_hook=OrderedDict))
				print "i:" + i
		delayDict[delay] = rawData
	# delayDict.pop(0)
	x = delayDict
	print "recv. x: "
	pprint(x)

	values = [ [[] for f in timeIntervals ] for _ in frameLengths]
	receivedData  = OrderedDict.fromkeys(frameLengths, None)
	j = 0
	k = 0
	l = 0
	tmp = 0
	for d in x:
		# print "d: " + str(d)
		for i in x[d]:
			# if i.keys()[0] not in receivedData:
			# 	receivedData[i.keys()[0]] = []
			if j == len(numberOfRuns):
				j = 0
				k += 1
			if tmp == len(numberOfRuns)*len(timeIntervals):
				tmp = 0
				k = 0
				j = 0
				l += 1
			# Get the binary rep. of the data
			values[l][k].append(i.values()[0].values()[0][0])
			j += 1
			tmp += 1
			#values.append(i.values()[0].values()[0][1])
			#values.append(re.sub(r'[\x00-\x1F,\x7F]+', '', i.values()[0].values()[0][1]))
		#values.append(re.sub(r'[\x00-\x1F]+', '', i.values()[0].values()[0][1]))

	print "receivedData: " + str(receivedData)
	print "values: " + str(values)
	i = 0
	for d in receivedData:
		print "d: " + str(d)
		receivedData[d] = values[i]
		i += 1
	print "received data: "
	pprint(receivedData)
	# exit()
	return timeIntervals, frameLengths, numberOfRuns, silentDelays, sentData, receivedData

def main():
	
	timeIntervals, frameLengths, numberOfRuns, silentDelays, sentData, receivedData = get_data()
	# print "timeIntervals"
	# print timeIntervals
	# print "frameLengths"
	# print frameLengths
	# print "numberOfRuns"
	# print numberOfRuns
	# print "sentData"
	# pprint(sentData)
	# print "receivedData"
	# print(json.dumps(receivedData, indent=4))
	# print receivedData
	# frameLengths = silentDelays
	errors = ["MSB", "EOM", "BF"]
	errorDict = dict.fromkeys(errors, dict.fromkeys(frameLengths, dict.fromkeys(timeIntervals, [0, 0])))
	# pprint(errorDict)
	# exit()
	globalMSB = 0
	frameLengthDict = dict.fromkeys(frameLengths, None)
	for frameLength in frameLengths:
		print "frameLength:" + str(frameLength)
		timeIntervalDict = dict.fromkeys(timeIntervals, 0)
		for interval in range(len(timeIntervals)):
			print "interval, timeIntervals[interval]"
			print interval, timeIntervals[interval]
			localMSB=0
			for run in range(len(numberOfRuns)):
				for bitValues in receivedData[frameLength][interval][run]:
					if bitValues == u'':
						localMSB += 1
						globalMSB += 1
			print "localMSB for: " + str(frameLength) + " " + str(timeIntervals[interval]) + " is: " + str(localMSB)
			timeIntervalDict[timeIntervals[interval]] = [localMSB, 0]
		frameLengthDict[frameLength] = timeIntervalDict
	errorDict["MSB"] = frameLengthDict
	print "globalMSB errors are:"
	print globalMSB
	# print "final errorDict with MSBs:"
	# pprint(errorDict)

	globalEOM = 0
	frameLengthDict = dict.fromkeys(frameLengths, None)
	for frameLength in frameLengths:
		print "frameLength:" + str(frameLength)
		timeIntervalDict = dict.fromkeys(timeIntervals, 0)
		for interval in range(len(timeIntervals)):
			print "interval, timeIntervals[interval]"
			print interval, timeIntervals[interval]
			localEOM=0
			shortFrames = []
			for run in range(len(numberOfRuns)):
				correctFrames = 0
				if len(receivedData[frameLength][interval][run]) < len(sentData[frameLength][interval][run]):
					print "received frames less than sent, got: " + str(len(receivedData[frameLength][interval][run]))
					print "sent frames: " + str(len(sentData[frameLength][interval][run]))
					shortFrames.append(len(receivedData[frameLength][interval][run]))
					for bitValues in receivedData[frameLength][interval][run]:
						if bitValues != u'':
							correctFrames += 1
					localEOM += 1
					globalEOM += 1
			print "localEOM for: " + str(frameLength) + " " + str(timeIntervals[interval]) + " is: " + str(localEOM)
			timeIntervalDict[timeIntervals[interval]] = [localEOM, shortFrames]
		frameLengthDict[frameLength] = timeIntervalDict
	errorDict["EOM"] = frameLengthDict
	print "globalEOM errors are:"
	print globalEOM
	print "final errorDict with EOMs:"
	pprint(errorDict)

	# Now analyze bitflip errors
	print "Bit-flip errors"
	globalBF = 0
	frameLengthDict = dict.fromkeys(frameLengths, None)
	for frameLength in frameLengths:
		print "frameLength:" + str(frameLength)
		timeIntervalDict = dict.fromkeys(timeIntervals, 0)
		for interval in range(len(timeIntervals)):
			print "interval, timeIntervals[interval]"
			print interval, timeIntervals[interval]
			localBF=0
			localReceivedFrames = 0
			localCorrectFrames = 0
			for run in range(len(numberOfRuns)):
				for receiverFrameIndex in range(len(receivedData[frameLength][interval][run])):
					if receivedData[frameLength][interval][run][receiverFrameIndex] == u'':
						continue
					else:
						localBF += Levenshtein.hamming(sentData[frameLength][interval][run][receiverFrameIndex],
													receivedData[frameLength][interval][run][receiverFrameIndex])
						globalBF += Levenshtein.hamming(sentData[frameLength][interval][run][receiverFrameIndex],
													receivedData[frameLength][interval][run][receiverFrameIndex])
						localReceivedFrames += 1
						if Levenshtein.hamming(sentData[frameLength][interval][run][receiverFrameIndex],
													receivedData[frameLength][interval][run][receiverFrameIndex]) == 0:
							localCorrectFrames += 1
			print "localBF for: " + str(frameLength) + " " + str(timeIntervals[interval]) + " is: " + str(localBF)
			timeIntervalDict[timeIntervals[interval]] = [localBF, localReceivedFrames, localCorrectFrames]
		frameLengthDict[frameLength] = timeIntervalDict
	errorDict["BF"] = frameLengthDict
	print "globalBF errors are:"
	print globalBF
	print "final errorDict with BFs:"
	pprint(errorDict)

	print "In percentage of total frames sent, the error rates are:"
	totalFrames = {7: 64, 14: 32, 28: 16}
	for error in errorDict:
		for frameLength in frameLengths:
			for interval in timeIntervals:
				if error == "MSB":
					errorDict[error][frameLength][interval][0] = float(errorDict[error][frameLength][interval][0])/(totalFrames[frameLength]*len(numberOfRuns)) * 100
				elif error == "EOM":
					errorDict[error][frameLength][interval][0] = float(errorDict[error][frameLength][interval][0])/len(numberOfRuns) * 100
				elif error == "BF":
					errorDict[error][frameLength][interval][0] = (float(errorDict[error][frameLength][interval][0])/float(errorDict[error][frameLength][interval][1]))/frameLength
					errorDict[error][frameLength][interval][2] = float(errorDict[error][frameLength][interval][2])/float(errorDict[error][frameLength][interval][1])*100
	pprint(errorDict)

main()