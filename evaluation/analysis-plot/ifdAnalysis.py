from pprint import pprint

frameLengths = [8, 15, 29]
timeIntervals = range(30, 110, 10)
bitTransmissionTimes = dict.fromkeys(frameLengths, dict.fromkeys(timeIntervals, 0))
interFrameDelays = dict.fromkeys(frameLengths, dict.fromkeys(timeIntervals, 0))

for f in frameLengths:
    print "frameLength: " + str(f)
    timeDict = dict.fromkeys(timeIntervals, [])
    for t in timeIntervals:
        print "timeInterval: " + str(t)
        txTime = t*f
        if txTime < 1000:
            ifd = 1000 - txTime
        elif txTime > 1000 and txTime < 2000:
            ifd = 2000 - txTime
        elif txTime > 2000 and txTime < 3000:
            ifd = 3000 - txTime
        # ifd = 1000 - txTime
        print "bitTransmissionTime: " + str(txTime)
        print "interFrameDelay: " + str(ifd)
        print "\n"
        timeDict[t] = [txTime, ifd]
    interFrameDelays[f] = timeDict

print "final ifd dict: "
pprint(interFrameDelays)